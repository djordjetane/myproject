#!/bin/sh

if [ -z "$QAAUTO_IP" ]; then
    echo "FAIL" > /qaauto/logs/qhauto-`date +"%Y-%m-%d"`.log
else
    echo "$QAAUTO_IP" > /qaauto/logs/qhauto-`date +"%Y-%m-%d"`.log
fi

