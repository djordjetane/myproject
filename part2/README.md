## Part 2

Solution to task is basicly in [cronjob.yml](https://gitlab.com/djordjetane/myproject/-/blob/main/part2/cronjob.yml) as 
workload was only requested.

It makes sense to have a persistant storage and most basic local hostPath PV and PVC configurations are located in [qalogs](https://gitlab.com/djordjetane/myproject/-/tree/main/part2/qalogs) directory.
This setup, makes sense for single node cluster (local testing)
