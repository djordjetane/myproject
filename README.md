# Task
Our QA department have developed a new test-automation platform. They have packaged it in a 
docker image and pushed it to our docker repository to make it available to the project for 
integrating with the CI/CD pipeline.
 
## Part 1
The pipeline should have a new Linux shell step added which to do the following:

1.	Pull the test-automation image [repository myproject, image qaauto, tag latest].
    Please assume that the login to the docker hub repository is handled outside this scenario.

2.	Run a new container from this image:

    - Attach a volume qalogs to the location /qaauto/logs;
    - Define environment variable QAAUTO_IP to be set to the IP of the host on which the build is running;
    - Please advise what other information you would need in order to run a container, please make assumptions about any missing information;

3.	Execute the script which runs the test automation, located on /qhauto/runauto.sh.

4.	A successful execution of the test automation will write a log file in the folder /qaauto/logs.

5.	The step should validate that and if the below 3 conditions are not satisfied, the step 
    should exist with a fail status:

    - A log file has been generated on /qaauto/logs location inside the container with name qhauto-yyyy-mm-dd.log;
    - The log file should not be empty;
    - The log file should not contain “FAIL” inside;

    Please make sure your script at the end stops and deletes the container, 
    deletes the image locally.
 
## Part 2
We would like to run the test automation every night at 2am on our Kubernetes deployment 
(same docker image and script to be executed).

Please advise what Kubernetes workload could be used and how.

